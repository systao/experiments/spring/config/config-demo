package fr.systao.experiments.config.client.network;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Provider {
	private OAuth oauth;
	private Basic basic;
	
	@Getter
	@Setter
	public static class OAuth {
		private String authorizationUrl;
		private String redirectUrl;
	  
	}
	
	@Getter
	@Setter	
	public static class Basic {
		private String username;
		private String password;
	}
}
