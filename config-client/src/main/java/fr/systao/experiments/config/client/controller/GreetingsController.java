package fr.systao.experiments.config.client.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.systao.experiments.config.client.network.Network;
import fr.systao.experiments.config.client.network.NetworkFactory;

@RefreshScope
@RestController
@RequestMapping(path="/greetings/{context}")
public class GreetingsController {
	@Autowired
	private NetworkFactory contextFactory;
	
	
	@Value("${systao.experiments.spring.config.demo.client.greetings.prefix}")
	private String prefix;
	
	@Value("${systao.experiments.spring.config.demo.title}")
	private String title;
	
	@GetMapping(path="/{name}")
	public Greetings greet(
			@PathVariable("context") String contextName,
			@PathVariable("name") String name) {
		Network network = contextFactory.getNetwork(contextName);
		return new Greetings(title, String.join(" ", prefix, name), network);
	}
}
