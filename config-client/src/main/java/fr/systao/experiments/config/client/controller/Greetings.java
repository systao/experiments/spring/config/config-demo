package fr.systao.experiments.config.client.controller;

import fr.systao.experiments.config.client.network.Network;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class Greetings {
	private String title;
	private String greet;
	private Network network;
}
