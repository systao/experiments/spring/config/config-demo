package fr.systao.experiments.config.client.network;

import java.io.InputStream;
import java.net.URL;
import java.util.Properties;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.YamlProcessor.ResolutionMethod;
import org.springframework.beans.factory.config.YamlPropertiesFactoryBean;
import org.springframework.boot.context.properties.bind.Binder;
import org.springframework.boot.context.properties.source.ConfigurationPropertySource;
import org.springframework.boot.context.properties.source.MapConfigurationPropertySource;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.core.io.InputStreamResource;
import org.springframework.stereotype.Component;


@Component
@RefreshScope
public class NetworkFactory {
	@Value("${systao.experiments.spring.config.demo.environment:dev}")
	private String environment;

	@Value("${spring.cloud.config.uri}")
	private String baseUrl;
	
	public Network getNetwork(String name) {
		Properties properties = loadProperties(name);

	    ConfigurationPropertySource source = new MapConfigurationPropertySource(properties);
        
        Binder binder = new Binder(source);
        
        return binder.bind("network", Network.class).get();
	}
	
	private Properties loadProperties(String network) {
		YamlPropertiesFactoryBean factory = new YamlPropertiesFactoryBean();
		factory.setResolutionMethod(ResolutionMethod.OVERRIDE);
		String url = String.join("", baseUrl, "/", network, "-", environment, ".yml");
		try ( InputStream inpustream = new URL(url).openStream() ) {
			factory.setResources(new InputStreamResource(inpustream));
		    factory.setSingleton(true);
		    factory.afterPropertiesSet();
			return factory.getObject();
		}
		catch ( Exception e ) {
			throw new RuntimeException(e);
		}
	}
}
