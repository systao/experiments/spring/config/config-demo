package fr.systao.experiments.config.client.network;

import java.util.Map;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class Network {
	private String name;
	private int id;
	private Map<String, Provider> providers;
}
