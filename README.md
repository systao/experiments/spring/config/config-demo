# Spring cloud config server demo

Demonstrate how to dynamically create custom beans from configuration managed by spring Cloud Server. This demo uses a Git backend. 

## Git repo setup

We use folders to properly organize resources. Application configuration and custom beans configuration are located in different folders. We need to let Spring know where the configurations are located using the searchPaths property. 

``` 
spring:
  cloud:
    config: 
      server:
        git:
          searchPaths: 
          - networks/**
          - applications/**
```

The applications folder contains the application configuration while the networks folder contains the custom bean configurations as shown below:

```
- applications
    |
    +-- config-client
          | 
          +-- config-client.yml
          +-- config-client-dev.yml
- networks
    |
    +-- network-1
          | 
          +-- network-1.yml
          +-- network-1-dev.yml
```

## Config client application

The client application exposes a Greetings endpoint at the following url:

```
/greetings/{network-id}/{name}
```

A Network object is created by the NetworkFactory. NetworkFactory explicitly fetches the Network instance configuration from the config server, creates a YamlPropertiesFactoryBean and bind it a new Network:

```
String url = String.join("", baseUrl, "/", 
     networkLogicalName, "-", environment, ".yml");
try ( InputStream inpustream = new URL(url).openStream() ) {
   ....		
}
```

## Config refresh 

Automatic configuration refresh is demonstrated in the GreetingsController. To enable configuration refresh, we need to:

- Enable the actuator refresh endpoint (see config-client.yml)
- Annotate the bean we want to refresh with @RefreshScope as is GreetingsController
- Trigger the refresh - f.i.

```
curl -X POST http://localhost:8080/management/refresh
```

Let's note that the management/* corresponds to actuator endpoints (base-path has been explicitly changed in the configuration -- default is 'actuator').
 
To automate the configuration we can make use of git hooks.



